
package com.wave.pagination.horizontalswiperefreshlayout;

import com.google.gson.annotations.SerializedName;

public class ListItem {

  @SerializedName("description")
  private String mDescription;
  @SerializedName("title")
  private String mTitle;

  public ListItem(String mDescription, String mTitle) {
    this.mDescription = mDescription;
    this.mTitle = mTitle;
  }

  public String getDescription() {
    return mDescription;
  }

  public void setDescription(String description) {
    mDescription = description;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

}
