package com.wave.pagination.horizontalswiperefreshlayout;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.wave.pagination.horizontalswiperefreshlayout.widget.HorizontalSwipeRefreshLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

  HorizontalSwipeRefreshLayout mSwipeRefreshLayout;
  RecyclerView mRecyclerView;
  LinearLayoutManager mLayoutManager;
  RecyclerViewAdapter mAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mSwipeRefreshLayout = findViewById(R.id.mHorizontalSwipeRefreshLayout);
    mRecyclerView = findViewById(R.id.mRecyclerView);
    setUp();

    mSwipeRefreshLayout.setOnRefreshListener(new HorizontalSwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        prepareDemoContent();
      }
    });
  }

  private void setUp() {
    mLayoutManager = new LinearLayoutManager(this);
    mLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mAdapter = new RecyclerViewAdapter(new ArrayList<ListItem>());
    /*
     * Prepare demo content
     */
    prepareDemoContent();
  }

  private void prepareDemoContent() {
    mSwipeRefreshLayout.setRefreshing(true);
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        mSwipeRefreshLayout.setRefreshing(false);
        ArrayList<ListItem> mSports = new ArrayList<>();
        String[] titleList = getResources().getStringArray(R.array.title_array);
        String[] infoList = getResources().getStringArray(R.array.info_array);

        for (int i = 0; i < titleList.length; i++) {
          mSports.add(new ListItem(infoList[i], titleList[i]));
        }
        mAdapter.addItems(mSports);
        mRecyclerView.setAdapter(mAdapter);
      }
    }, 2000);

  }
}
