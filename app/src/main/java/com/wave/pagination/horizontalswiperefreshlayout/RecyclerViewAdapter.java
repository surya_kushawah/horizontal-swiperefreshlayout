package com.wave.pagination.horizontalswiperefreshlayout;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created on : May 04, 2019 Author     : AndroidWave
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

  private List<ListItem> mListItems;

  public RecyclerViewAdapter(List<ListItem> postItems) {
    this.mListItems = postItems;
  }


  @Override
  public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
    holder.onBind(position);
  }

  @Override
  public int getItemViewType(int position) {
    return 0;
  }

  @Override
  public int getItemCount() {
    return mListItems.size();
  }

  public void addItems(List<ListItem> listItems) {
    mListItems = listItems;
    notifyDataSetChanged();
  }

  public class ViewHolder extends BaseViewHolder {

    TextView textViewTitle;
    TextView textViewDescription;


    ViewHolder(View itemView) {
      super(itemView);
      textViewTitle = itemView.findViewById(R.id.tvTitle);
      textViewDescription = itemView.findViewById(R.id.tvInfo);
    }

    protected void clear() {

    }

    public void onBind(int position) {
      super.onBind(position);
      ListItem item = mListItems.get(position);
      textViewTitle.setText(item.getTitle());
      textViewDescription.setText(item.getDescription());
    }
  }

}
